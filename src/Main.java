public class Main {
    public static void main(String[] args){
        System.out.println("PREPARACION DE LASAGNA");
        System.out.println("-----------------------");
        PrepararAlimento lasagna=new Lasagna(new Carne());
        lasagna.obtener();
        System.out.println("-----------------------");
        lasagna.setImplementador(new Verduras());
        lasagna.obtener();
    }
}
