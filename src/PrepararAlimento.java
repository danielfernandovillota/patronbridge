public abstract class PrepararAlimento {

    Cocinar implementador;

    public  Cocinar getImplementador(){

        return this.implementador;
    }

    public void setImplementador(Cocinar implementador){
        this.implementador=implementador;
    }

    public abstract void obtener();
}
