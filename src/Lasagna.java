public class Lasagna extends PrepararAlimento {

    public Lasagna(Cocinar implementador){
        this.setImplementador(implementador);
    }

    @Override
    public void obtener() {
        System.out.println("Preparando lasagna");
        getImplementador().elaborar();
    }


}
